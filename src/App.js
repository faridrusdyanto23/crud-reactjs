// import 'bootstrap/dist/css/bootstrap.css';
// import logo from './logo.svg';
// import './App.css';
import {BrowserRouter as Router, Route, } from 'react-router-dom';
import React from 'react';
import TambahData from './components/add-data/TambahData';
import EditComp from './components/EditComp/EditComp';
import Mahasiswa from './components/data-mahasiswa/Mahasiswa';
import NavbarComp from './components/NavbarComp';

function App() {
  return (
    <Router>
      <NavbarComp />
      <Route exact path="/" component={Mahasiswa} />
      <Route exact path="/mahasiswa/add" component={TambahData} />
      <Route exact path="/mahasiswa/edit" component={EditComp} />
    </Router>
  );
}

export default App;
