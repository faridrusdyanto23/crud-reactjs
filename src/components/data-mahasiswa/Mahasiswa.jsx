import axios from 'axios';
import qs from 'querystring';
import { PureComponent } from 'react';
import { Button, Container, NavLink, Alert, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const api = 'http://localhost:3030';

class Mahasiswa extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      mahasiswa: [],
      response: '',
      display: false
    }
  }

  componentDidMount() {
    axios.get(`${api}/tampil`).then(res => {
      this.setState({
        mahasiswa: res.data.values
      })
    })
  }

  deleteMahasiswa = (idMahasiswa) => {
    const {mahasiswa} = this.state;
    const data = qs.stringify({
      id_mahasiswa: idMahasiswa,
    })
    axios.delete(`${api}/hapus`,
      {
        data: data,
        headers: {'Content-type': 'application/x-www-form-urlencoded'}
      }
    ).then(json => {
      if(json.data.status === 200) {
        this.setState({
          response: json.data.values,
          mahasiswa:mahasiswa.filter(mahasiswa => mahasiswa.id_mahasiswa !== idMahasiswa),
          display: true
        })
        this.props.history.push('/')
      } else {
        this.setState({
          response: json.data.values,
          display: true
        })
        // this.props.history.push('/mahasiswa')
      }
    })
  }


  render() {
    return (
      <Container>
        <br/>
        <h4>Data Mahasiswa</h4>
        <Alert variant="success" show={this.state.display}>
            {this.state.response}
        </Alert>
        <br/>
        <NavLink href="/mahasiswa/add"><Button variant="success">Tambah Data</Button></NavLink>
          <hr/>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th><center>NIM</center></th>
              <th><center>Nama</center></th>
              <th><center>Jurusan</center></th>
              <th><center>Aksi</center></th>
            </tr>
          </thead>
          <tbody>
            {this.state.mahasiswa.map(mahasiswa =>
              <tr key={mahasiswa.id_mahasiswa}>
                <td align="center">{mahasiswa.nim}</td>
                <td>{mahasiswa.nama}</td>
                <td>{mahasiswa.jurusan}</td>
                <td align="center">
                  <Link to = {
                    {
                      pathname: '/mahasiswa/edit',
                      state: {
                        id_mahasiswa: mahasiswa.id_mahasiswa,
                        nim: mahasiswa.nim,
                        nama: mahasiswa.nama,
                        jurusan: mahasiswa.jurusan
                      }
                    }
                  }>
                    <Button>Edit</Button>{' '}
                  </Link>
                  <Button variant="danger" onClick={() => this.deleteMahasiswa(mahasiswa.id_mahasiswa)}>Hapus</Button>
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      </Container>
    )
  }
}
export default Mahasiswa;