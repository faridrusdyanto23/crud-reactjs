import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';


const NavbarComp = () => {

  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="white">
        <Container>  
          <Navbar.Brand href="/">Mahasiswa</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#features">Features</Nav.Link>
              <Nav.Link href="#pricing">Pricing</Nav.Link>
              <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                <NavDropdown.Item href="/reducer">Reducer</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">End</NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
            <Nav.Link href="#">Hooks</Nav.Link>
              <Nav.Link eventKey={2} href="/">Data Mahasiswa</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

export default NavbarComp;