import React, { Component } from 'react'
import axios from 'axios';
import { Container, Alert, Button, Form } from 'react-bootstrap';
import NavbarComp from '../NavbarComp';

const api = 'http://localhost:3030';

class TambahData extends Component {
  constructor(props) {
    super(props)

    this.state = {
      nim: '',
      nama: '',
      jurusan: '',
      response: '',
      display: false
    }
  }

  handleChange = (e) => {
    this.setState({[e.target.name] : e.target.value})
  }

  addMahasiswa = () => {
    axios.post(`${api}/tambah`, {
      nim: this.state.nim,
      nama: this.state.nama,
      jurusan: this.state.jurusan
    }).then(json => {
      if(json.data.status === 200) {
        this.setState({
          response: json.data.values,
          display: true
        })
      } else {
        this.setState({
          response: json.data.values,
          display: true
        })
      }
    })
  }

  render() {
    return (
      <div>
        <Container>
          <br/>
          <h4>Form Tambah Data</h4>
          <Alert variant="success" show={this.state.display}>
            {this.state.response}
          </Alert>
          <Form>
              <Form.Group controlId="formBasicNIM">
                <Form.Label>NIM</Form.Label>
                <Form.Control type="text" name="nim" value={this.state.nim} onChange={this.handleChange} placeholder="masukan nim"></Form.Control>
              </Form.Group>
              <Form.Group controlId="formBasicName">
                <Form.Label>Nama</Form.Label>
                <Form.Control type="text" name="nama" value={this.state.nama} onChange={this.handleChange} placeholder="masukan nama"></Form.Control>
              </Form.Group>
              <Form.Group controlId="formBasicJurusan">
                <Form.Label>Jurusan</Form.Label>
                <Form.Control type="text" name="jurusan" value={this.state.jurusan} onChange={this.handleChange} placeholder="masukan jurusan"></Form.Control>
              </Form.Group>
              <Button variant="primary" type="button" onClick={this.addMahasiswa}>Submit</Button>
          </Form>
        </Container>
      </div>
    )
  }
}

export default TambahData;